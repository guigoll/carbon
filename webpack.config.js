const path = require('path');

module.exports = require('carbon-factory/webpack.config')({
  parcelifyPaths: [path.resolve(process.cwd(), './node_modules/carbon-react')],
  singlePageApp: true
});
