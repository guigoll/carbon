import React from 'react';
import 'carbon-react/lib/utils/css';
import { Route } from 'react-router';
import { startRouter } from 'carbon-react/lib/utils/router';

// import our component we want to render
import Sample from 'components/sample';

// defined what components get renderered per route - see docs for React Router for more info
const routes = (
  <Route path='/' component={ Sample } />
);

// start the router with the given routes
startRouter(routes);

// this starts hot reloading for webpack
if (module.hot) {
  module.hot.accept();
}
